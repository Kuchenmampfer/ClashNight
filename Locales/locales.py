import json
from pathlib import Path


def process_dict(dictionary: dict, locale: str, pass_dict=None) -> dict:
    if pass_dict is None:
        new_dict = {}
    else:
        new_dict = pass_dict
    for key, value in dictionary.items():
        try:
            key = int(key)
        except ValueError:
            pass
        if isinstance(value, dict):
            new_dict[key] = process_dict(value, locale, pass_dict if pass_dict is None else pass_dict[key])
        else:
            if new_dict.get(key) is None:
                new_dict[key] = {locale: value}
            else:
                new_dict[key][locale] = value
    return new_dict


def get_data(py_file: str) -> dict:
    with open('Locales/en-GB.json', 'r') as f:
        json_data = json.load(f)
        data = process_dict(json_data.get(py_file), 'lt')
        data = process_dict(json_data.get(py_file), 'zh-TW', data)
    for directory in Path('Locales').iterdir():
        if directory.is_dir():
            for file in directory.glob('*.json'):
                with file.open('r') as f:
                    name = directory.name
                    json_data = json.load(f)
                    data = process_dict(json_data.get(py_file), name, data)
    return data
