from pathlib import Path
from typing import Optional

import coc
import discord
from discord import Option, OptionChoice
from discord.ext import commands

from Locales import locales
from cogs.utils.leaderboard import Leaderboard, Objectify

FORMAT_STRINGS = [
    '{0}`{1.best_builder_base_trophies}`🏆 {1.coc_name}\n',
    '{0}`{1.best_season_id}`🗓️ `{1.best_season_rank:4}`🪜 `{1.best_season_trophies}`🏆 {1.coc_name}\n',
    '{0}`{1.best_season_id}`🗓️ `{1.best_season_rank:4}`🪜 `{1.best_season_trophies}`🏆 {1.coc_name}\n',
    '{0}`{1.best_season_id}`🗓️ `{1.best_season_rank:4}`🪜 `{1.best_season_trophies}`🏆 {1.coc_name}\n',
    '{0}`{1.legend_cups}`🏅 {1.coc_name}\n',
    '{0}`{1.trophies}`🏆 {1.coc_name}\n',
    '{0}`{1.wins}`🎯 {1.coc_name}\n',
    '{0}`{1.builder_halls}`🛖 {1.coc_name}\n',
]

LOCALIZATION = locales.get_data('leaderboards')

DATA_COLUMNS = [
    'best_versus_trophies DESC',
    'best_season_id, best_season_rank, best_season_trophies DESC',
    'best_season_rank, best_season_trophies DESC',
    'best_season_trophies DESC, best_season_rank'
]


def get_location(ctx: discord.AutocompleteContext):
    path = Path(f'Locales/{ctx.interaction.locale}/locations.csv')
    if not path.exists():
        path = Path('locations.csv')
    with open(path, 'r', encoding='utf-8') as f:
        locations = []
        for line in f.readlines():
            line = line.replace('"', '')
            loc, _ = line.split(',')
            locations.append(loc)
    return [location for location in locations if location.lower().startswith(ctx.value.lower())]


class Leaderboards(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    leaderboard = discord.SlashCommandGroup('leaderboard', 'Get various leaderboards')

    @leaderboard.command(name='local', description='Get a live leaderboard from any location',
                         description_localizations=LOCALIZATION['local']['description'])
    async def local_lb(self,
                       ctx: discord.ApplicationContext,
                       location: Option(
                           str,
                           'Choose the location you want the leaderboard of.',
                           name_localizations=LOCALIZATION['local']['location_option']['name'],
                           description_localizations=LOCALIZATION['local']['location_option']['description'],
                           autocomplete=get_location
                       ),
                       mode: Option(str,
                                    'Do you want the player or clan leaderboard? Default: Players',
                                    choices=[OptionChoice(
                                        'Players',
                                        name_localizations=LOCALIZATION['common_option']['mode']['choices']['Players']
                                    ), OptionChoice(
                                        'Clans',
                                        name_localizations=LOCALIZATION['common_option']['mode']['choices']['Clans']
                                    )],
                                    default='Players',
                                    name_localizations=LOCALIZATION['common_option']['mode']['name'],
                                    description_localizations=LOCALIZATION['common_option']['mode']['description'],
                                    )
                       ):
        await ctx.defer()
        path = Path(f'Locales/{ctx.interaction.locale}/locations.csv')
        if not path.exists():
            path = Path('locations.csv')
        with open(path, 'r', encoding='utf-8') as f:
            location_dict: dict[str, Optional[int]] = {}
            for line in f.readlines():
                line = line.replace('"', '')
                loc, loc_id = line.split(',')
                location_dict[loc.lower()] = int(loc_id)
        try:
            if mode == 'Players':
                leaderboard_list: list[coc.RankedPlayer] = \
                    await self.bot.coc.get_location_players_builder_base(location_dict[location.lower()])
                leaderboard = Leaderboard(ctx.author.id, leaderboard_list,
                                          LOCALIZATION['local']['embed_header']['Players'][ctx.locale].format(location),
                                          '{0}{1}`{2.builder_base_trophies}`🏆 {2.name}\n',
                                          True
                                          )
            else:
                leaderboard_list: list[coc.RankedClan] = \
                    await self.bot.coc.get_location_clans_builder_base(location_dict[location.lower()])
                leaderboard = Leaderboard(ctx.author.id, leaderboard_list,
                                          LOCALIZATION['local']['embed_header']['Clans'][ctx.locale].format(location),
                                          '{0}{1}`{2.builder_base_points}`🏆 {2.name}\n',
                                          True
                                          )
        except coc.GatewayError:
            return
        await leaderboard.disable_buttons()
        if len(leaderboard.embeds) > 1:
            message = await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index], view=leaderboard)
            await leaderboard.wait()
            await message.edit(view=None)
        else:
            await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index])

    @leaderboard.command(name='global', description='Get a global live leaderboard',
                         description_localizations=LOCALIZATION['global']['description'])
    async def global_lb(self,
                        ctx: discord.ApplicationContext,
                        mode: Option(str,
                                     'Do you want the player or clan leaderboard? Default: Players',
                                     choices=[OptionChoice(
                                         'Players',
                                         name_localizations=LOCALIZATION['common_option']['mode']['choices']['Players']
                                     ), OptionChoice(
                                         'Clans',
                                         name_localizations=LOCALIZATION['common_option']['mode']['choices']['Clans']
                                     )],
                                     default='Players',
                                     name_localizations=LOCALIZATION['common_option']['mode']['name'],
                                     description_localizations=LOCALIZATION['common_option']['mode']['description'],
                                     )
                        ):
        await ctx.defer()
        try:
            if mode == 'Players':
                leaderboard_list: list[coc.RankedPlayer] = \
                    await self.bot.coc.get_location_players_builder_base()
                leaderboard = Leaderboard(ctx.author.id, leaderboard_list,
                                          LOCALIZATION['global']['embed_header']['Players'][ctx.locale],
                                          '{0}{1}`{2.builder_base_trophies}`🏆 {2.name}\n',
                                          True
                                          )
            else:
                leaderboard_list: list[coc.RankedClan] = \
                    await self.bot.coc.get_location_clans_builder_base()
                leaderboard = Leaderboard(ctx.author.id, leaderboard_list,
                                          LOCALIZATION['global']['embed_header']['Clans'][ctx.locale],
                                          '{0}{1}`{2.builder_base_points}`🏆 {2.name}\n',
                                          True
                                          )
        except coc.GatewayError:
            return
        await leaderboard.disable_buttons()
        if len(leaderboard.embeds) > 1:
            message = await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index], view=leaderboard)
            await leaderboard.wait()
            await message.edit(view=None)
        else:
            await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index])

    @leaderboard.command(name='server',
                         description='Get a leaderboard with all the players in this discord server',
                         description_localizations=LOCALIZATION['server']['description'])
    async def server_lb(self,
                        ctx: discord.ApplicationContext,
                        category: Option(int,
                                         'In which category shall I rank the server members? Default: Trophies',
                                         choices=[
                                             OptionChoice(
                                                 'Trophies', 5,
                                                 name_localizations=LOCALIZATION['header_categories'][5]),
                                             OptionChoice(
                                                 'All time best', 0,
                                                 name_localizations=LOCALIZATION['header_categories'][0]),
                                             OptionChoice(
                                                 'Best finish season', 1,
                                                 name_localizations=LOCALIZATION['header_categories'][1]),
                                             OptionChoice(
                                                 'Best finish rank', 2,
                                                 name_localizations=LOCALIZATION['header_categories'][2]),
                                             OptionChoice(
                                                 'Best finish trophies', 3,
                                                 name_localizations=LOCALIZATION['header_categories'][3]),
                                             OptionChoice(
                                                 'Number of wins', 6,
                                                 name_localizations=LOCALIZATION['header_categories'][6]),
                                             OptionChoice(
                                                 'Number of builder halls destroyed', 7,
                                                 name_localizations=LOCALIZATION['header_categories'][7]),
                                         ],
                                         default=5,
                                         name_localizations=LOCALIZATION['server']['category_option']['name'],
                                         description_localizations=
                                         LOCALIZATION['server']['category_option']['description']
                                         ),
                        ):
        await ctx.defer()
        async with self.bot.pool.acquire() as conn:
            records = await conn.fetch(
                '''
                SELECT * FROM FullCocPlayers
                WHERE coc_tag IN
                (
                    SELECT coc_tag FROM RegisteredBuilderBasePlayers
                    WHERE discord_member_id IN
                    (
                        SELECT member_id FROM GuildMemberReferences
                        WHERE guild_id = $1
                    )
                )
                ''',
                ctx.guild_id
            )
            if len(records) == 0:
                await ctx.respond(embed=discord.Embed(colour=discord.Colour.red(),
                                                      description=LOCALIZATION['server']['nothing_linked'][ctx.locale]))
                return

            leaderboard_list = []
            for record in records:
                dict_player = dict(record)
                dict_player['sort_by'] = record[category + 2]
                if not dict_player['sort_by']:
                    continue
                player = Objectify(dict_player)
                leaderboard_list.append(player)
            leaderboard_list.sort(key=lambda x: x.sort_by, reverse=category in [0, 3, 4, 5, 6, 7])

        leaderboard = Leaderboard(ctx.author.id, leaderboard_list,
                                  LOCALIZATION['server']['embed_header'][ctx.locale].format(
                                      category=LOCALIZATION['header_categories'][category][ctx.locale],
                                      server=ctx.guild.name
                                  ),
                                  FORMAT_STRINGS[category],
                                  False
                                  )
        await leaderboard.disable_buttons()
        if len(leaderboard.embeds) > 1:
            message = await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index], view=leaderboard)
            await leaderboard.wait()
            await message.edit(view=None)
        else:
            await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index])

    @leaderboard.command(name='database',
                         description='Get a leaderboard with all the players in my database',
                         description_localizations=LOCALIZATION['database']['description'])
    async def database_lb(self,
                          ctx: discord.ApplicationContext,
                          category: Option(int,
                                           'In which category shall I rank the players? Default: All time best',
                                           choices=[
                                               OptionChoice(
                                                   'All time best', 0,
                                                   name_localizations=LOCALIZATION['header_categories'][0]),
                                               OptionChoice(
                                                   'Best finish season', 1,
                                                   name_localizations=LOCALIZATION['header_categories'][1]),
                                               OptionChoice(
                                                   'Best finish rank', 2,
                                                   name_localizations=LOCALIZATION['header_categories'][2]),
                                               OptionChoice(
                                                   'Best finish trophies', 3,
                                                   name_localizations=LOCALIZATION['header_categories'][3]),
                                           ],
                                           default=0,
                                           name_localizations=LOCALIZATION['database']['category_option']['name'],
                                           description_localizations=
                                           LOCALIZATION['database']['category_option']['description']
                                           ),
                          ):
        await ctx.defer()
        leaderboard_list = []

        async with self.bot.pool.acquire() as conn:
            records = await conn.fetch(
                f'''
                SELECT * FROM TopBuilderBasePlayers
                WHERE best_season_id IS NOT NULL
                ORDER BY {DATA_COLUMNS[category]} 
                LIMIT 500
                '''
            )
        for record in records:
            dict_player = dict(record)
            player = Objectify(dict_player)
            leaderboard_list.append(player)

        leaderboard = Leaderboard(ctx.author.id, leaderboard_list,
                                  LOCALIZATION['database']['embed_header'][ctx.locale].format(
                                      category=LOCALIZATION['header_categories'][category][ctx.locale]),
                                  FORMAT_STRINGS[category],
                                  False
                                  )
        await leaderboard.disable_buttons()
        if len(leaderboard.embeds) > 1:
            message = await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index], view=leaderboard)
            await leaderboard.wait()
            await message.edit(view=None)
        else:
            await ctx.respond(embed=leaderboard.embeds[leaderboard.current_embed_index])


def setup(bot):
    bot.add_cog(Leaderboards(bot))
