from datetime import timezone

import discord
from coc.utils import get_season_start, get_season_end
from discord.ext import commands

from Locales import locales

LOCALIZATION = locales.get_data('season')


class SeasonView(discord.ui.View):
    def __init__(self, locale: str):
        self.locale = locale
        self.month = get_season_start().month
        self.year = get_season_start().year
        self.embed = None
        self._update_embed()
        self.previous_button = discord.ui.Button(label='◀️', style=discord.ButtonStyle.green)
        self.previous_button.callback = self.go_to_previous
        self.next_button = discord.ui.Button(label='▶️', style=discord.ButtonStyle.green)
        self.next_button.callback = self.go_to_next
        super().__init__(self.previous_button, self.next_button)

    def _update_embed(self):
        description = LOCALIZATION['embed_description']['Start'][self.locale]
        description += f': <t:{int(get_season_start(self.month, self.year).replace(tzinfo=timezone.utc).timestamp())}>\n'
        description += LOCALIZATION['embed_description']['End'][self.locale]
        description += f': <t:{int(get_season_end(self.month, self.year).replace(tzinfo=timezone.utc).timestamp())}>\n'
        description += LOCALIZATION['embed_description']['Or'][self.locale]
        description += f': <t:{int(get_season_end(self.month, self.year).replace(tzinfo=timezone.utc).timestamp())}:R>\n'
        duration = (get_season_end(self.month, self.year) - get_season_start(self.month, self.year)).days
        description += LOCALIZATION['embed_description']['Duration'][self.locale].format(duration)
        description += LOCALIZATION['embed_description']['Trophies'][self.locale]
        description += f': {"7100 (+-60)" if duration < 31 else "7330 (+-90)"}\n'
        description += LOCALIZATION['embed_description']['Clan Points'][self.locale]
        description += f': {"68000 (+-1200)" if duration < 31 else "70500 (+-1500)"}\n'
        self.embed = discord.Embed(
            title=LOCALIZATION['embed_title'][self.locale].format(year=self.year if self.month < 12 else self.year + 1,
                                                                  month=self.month + 1 if self.month < 12 else 1),
            description=description,
            colour=discord.Colour.blue()
        )

    async def _update_message(self, interaction: discord.Interaction):
        self._update_embed()
        await interaction.response.edit_message(view=self, embed=self.embed)

    async def go_to_previous(self, interaction: discord.Interaction):
        if self.month == 1:
            self.month = 12
            self.year -= 1
        else:
            self.month -= 1
        await self._update_message(interaction)

    async def go_to_next(self, interaction: discord.Interaction):
        if self.month == 12:
            self.month = 1
            self.year += 1
        else:
            self.month += 1
        await self._update_message(interaction)


class Season(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(description='Get info on the current season',
                            description_localizations=LOCALIZATION['command_description'])
    async def season(self, ctx: discord.ApplicationContext):
        await ctx.defer()
        view = SeasonView(ctx.locale)
        message = await ctx.respond(view=view, embed=view.embed)
        await view.wait()
        await message.edit(view=None)


def setup(bot):
    bot.add_cog(Season(bot))
