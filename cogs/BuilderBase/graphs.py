import discord.ui
import matplotlib
import pytz
from discord import Option, OptionChoice
from discord.ext import commands

from Locales import locales
from cogs.utils.TimePlot import TimePlot
from cogs.utils.scroll_view import ScrollView

matplotlib.style.use('cogs/utils/neon.mplstyle')

SQL_DICT = {
    'Trophies': 'SELECT time, trophies FROM BuilderBaseBoard WHERE coc_tag = $1 ORDER BY time',
    'Builder Halls': 'SELECT time, builder_halls FROM BuilderBaseBoard WHERE coc_tag = $1 ORDER BY time',
    'Winrate': 'SELECT time, 10 * (wins - FIRST_VALUE(wins) OVER (ORDER BY time ROWS 9 PRECEDING)) '
               'FROM BuilderBaseBoard WHERE coc_tag = $1 ORDER BY time',
    'Trophy Change': 'SELECT time, (trophies - FIRST_VALUE(trophies) OVER (ORDER BY time ROWS 9 PRECEDING)) '
                     'FROM BuilderBaseBoard WHERE coc_tag = $1 ORDER BY time'
}

LOCALIZATION = locales.get_data('graphs')


class Graphs(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(description='View graphs how your trophy pushing went',
                            description_localizations=LOCALIZATION['my']['description'])
    async def my(self, ctx: discord.ApplicationContext,
                 which_data: Option(
                     str,
                     name='category',
                     name_localizations=LOCALIZATION['my']['options']['which_data']['name'],
                     description='Which data shall I plot over time?',
                     description_localizations=LOCALIZATION['my']['options']['which_data']['description'],
                     default='Trophies',
                     choices=[
                         OptionChoice('Trophies',
                                      name_localizations=LOCALIZATION['my']['options']
                                      ['which_data']['choices']['Trophies']),
                         OptionChoice('Builder Halls',
                                      name_localizations=LOCALIZATION['my']['options']
                                      ['which_data']['choices']['Builder Halls']),
                         OptionChoice('Trophy Change',
                                      name_localizations=LOCALIZATION['my']['options']
                                      ['which_data']['choices']['Trophy Change']),
                     ]),
                 time_interval: Option(
                     float,
                     name='time-interval',
                     name_localizations=LOCALIZATION['common_option']['time_interval']['name'],
                     description='How many days of data do you want to see at once? Default: 7.0',
                     description_localizations=LOCALIZATION['common_option']['time_interval']['description'],
                     min_value=0.04, max_value=365, default=7.0)
                 ):
        await ctx.defer()
        plot = TimePlot(LOCALIZATION['my']['plot_headline'][ctx.locale].format(
            LOCALIZATION['my']['options']['which_data']['choices'][which_data][ctx.locale],
            ctx.author.display_name),
                        time_interval,
                        True,
                        LOCALIZATION['my']['y_labels'][which_data][ctx.locale],
                        pytz.timezone(LOCALIZATION['my']['timezone'][ctx.locale]))
        async with self.bot.pool.acquire() as conn:
            tag_records = await conn.fetch('''
                                           SELECT r.coc_tag, t.coc_name
                                           FROM RegisteredBuilderBasePlayers AS r
                                           JOIN TopBuilderBasePlayers AS t ON r.coc_tag = t.coc_tag
                                           WHERE r.discord_member_id = $1
                                           ''',
                                           ctx.author.id)
            if len(tag_records) == 0:
                await ctx.respond(LOCALIZATION['my']['nothing_linked'][ctx.locale])
                return
            nothing_to_show = True
            for record in tag_records:
                tag = record[0]
                name = record[1]
                data_records = await conn.fetch(SQL_DICT[which_data], tag)
                if len(data_records) > 1:
                    nothing_to_show = False
                times = [record[0] for record in data_records]
                data = [record[1] for record in data_records]
                plot.add_data(tag, name, times, data)
            if nothing_to_show:
                await ctx.respond(LOCALIZATION['my']['no_attacks_tracked'][ctx.locale])
                return
            url = plot.plot2url()
        view = ScrollView(plot, ctx.user.id, ctx.locale)
        message = await ctx.respond(url, view=view)
        await view.wait()
        await message.edit(view=None)

    @commands.slash_command(description='How active are the top 200 currently?',
                            description_localizations=LOCALIZATION['activity']['description'])
    async def activity(
            self,
            ctx: discord.ApplicationContext,
            time_interval: Option(
                float,
                name='time-interval',
                name_localizations=LOCALIZATION['common_option']['time_interval']['name'],
                description='How many days of data do you want to see at once? Default: 7.0',
                description_localizations=LOCALIZATION['common_option']['time_interval']['description'],
                min_value=0.04, max_value=365, default=7.0)
    ):
        await ctx.defer()
        plot = TimePlot('Activity of tracked players', time_interval, False,
                        'Number of trophy changes of tracked players per hour')
        async with self.bot.pool.acquire() as conn:
            records = await conn.fetch('''
                                       SELECT hour, COALESCE(activity, 0)
                                       FROM (
                                           SELECT generate_series(
                                               (SELECT date_trunc('HOUR', min(time)) FROM BuilderBaseBoard)::timestamp,
                                               (SELECT max(time) FROM BuilderBaseBoard)::timestamp,
                                               '1 hour'::interval) AS hour
                                           ) Hours
                                       LEFT JOIN (
                                           SELECT date_trunc('HOUR', time) trunc, COUNT(*) activity
                                           FROM BuilderBaseboard
                                           WHERE is_season_end = FALSE
                                           GROUP BY date_trunc('HOUR', time)
                                           )  Activities ON trunc = hour
                                       ORDER BY hour
                                       ''')
        times = [record[0] for record in records]
        activities = [record[1] for record in records]
        plot.add_data('Apfelkuchen', 'Changes per hour', times, activities)
        async with self.bot.pool.acquire() as conn:
            records = await conn.fetch('''
                                           SELECT day, COALESCE(activity, 0) / 24
                                           FROM (
                                               SELECT generate_series(
                                                   (SELECT date_trunc('DAY', min(time)) FROM BuilderBaseBoard)::timestamp,
                                                   (SELECT max(time) FROM BuilderBaseBoard)::timestamp,
                                                   '1 day'::interval) AS day
                                               ) Days
                                           LEFT JOIN (
                                               SELECT date_trunc('DAY', time) trunc, COUNT(*) activity
                                               FROM BuilderBaseboard
                                               WHERE is_season_end = FALSE
                                               GROUP BY date_trunc('DAY', time)
                                               )  Activities ON trunc = day
                                           ORDER BY day
                                           ''')
        times = [record[0] for record in records]
        activities = [record[1] for record in records]
        plot.add_data('Birnenkuchen', 'Daily average', times, activities)
        url = plot.plot2url()
        view = ScrollView(plot, ctx.user.id, ctx.locale)
        message = await ctx.respond(url, view=view)
        await view.wait()
        await message.edit(view=None)

    @commands.slash_command(name='trophy-level', description='Plot the global best trophies over time')
    async def trophy_level(
            self,
            ctx: discord.ApplicationContext,
            time_interval: Option(
                float,
                name='time-interval',
                name_localizations=LOCALIZATION['common_option']['time_interval']['name'],
                description='How many days of data do you want to see at once? Default: 7.0',
                description_localizations=LOCALIZATION['common_option']['time_interval']['description'],
                min_value=0.04, max_value=365, default=7.0)
    ):
        await ctx.defer()
        plot = TimePlot(LOCALIZATION['trophy_level']['plot_headline'][ctx.locale], time_interval, False,
                        LOCALIZATION['trophy_level']['plot_legend'][ctx.locale])
        async with self.bot.pool.acquire() as conn:
            records = await conn.fetch('''
                                       SELECT time, trophies[1]
                                       FROM BuilderBaseTopTen
                                       WHERE region = 'global'
                                       ORDER BY time
                                       ''')
        times = [record[0] for record in records]
        first_place = [record[1] for record in records]
        plot.add_data('Apfelkuchen', LOCALIZATION['trophy_level']['plot_data'][ctx.locale], times, first_place)
        url = plot.plot2url()
        view = ScrollView(plot, ctx.user.id, ctx.locale)
        message = await ctx.respond(url, view=view)
        await view.wait()
        await message.edit(view=None)


def setup(bot):
    bot.add_cog(Graphs(bot))
