import csv

import discord
from aioconsole import aexec
from discord.ext import commands
from prettytable import PrettyTable


class RunStuff(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def cog_check(self, ctx):
        return ctx.guild.id in [805155951324692571, 949752401978589314] and ctx.author.id == 471752948200898561

    dev = discord.SlashCommandGroup(name='dev', guild_ids=[805155951324692571])

    @commands.is_owner()
    @dev.command(name='eval')
    async def eval(self, ctx, *, code: str):
        try:
            await aexec(code + "\nawait ctx.respond(response)", {"bot": self.bot, "ctx": ctx})
        except BaseException as e:
            await ctx.respond(e)

    @dev.command(name='sql_fetch')
    async def sql_fetch(self, ctx, *, sql_query):
        async with self.bot.pool.acquire() as conn:
            try:
                records = await conn.fetch(sql_query)
                table = PrettyTable()
                table.field_names = [key for key in records[0].keys()]
                table.add_rows(records)
                table_str = table.get_string()
                with open('table.txt', 'w', encoding='utf-8') as f:
                    f.write(table_str)
                await ctx.respond(file=discord.File('table.txt'))
            except BaseException as e:
                await ctx.respond(e)

    @dev.command(name='reload')
    async def reload(self, ctx, cog):
        name = 'cogs.' + cog
        try:
            self.bot.reload_extension(name)
            await ctx.respond(f'`{cog}` wurde neu geladen :white_check_mark:')
        except commands.ExtensionError as e:
            await ctx.respond(e)

    @dev.command(name='guild-member-update')
    async def guild_member_update(self, ctx, guild: discord.Guild):
        async with self.bot.pool.acquire() as conn:
            db_members = await conn.fetch('SELECT member_id FROM DiscordMembers')
            members = []
            for member in db_members:
                if guild.get_member(member[0]) is not None:
                    members.append(member[0])
            await conn.executemany('INSERT INTO GuildMemberReferences(guild_id, member_id) VALUES($1, $2)',
                                   [[guild.id, member_id] for member_id in members])
            await ctx.respond('Member references updated :white_check_mark:')

    @commands.is_owner()
    @dev.command(name='export-top-200-history', guild_ids=[805155951324692571])
    async def export_top_200_history(self, ctx: discord.ApplicationContext):
        await ctx.defer()
        message = await ctx.respond('Selecting...')
        async with self.bot.pool.acquire() as conn:
            records = await conn.fetch('''
                                       SELECT *
                                       FROM BuilderBaseLeaderboards
                                       WHERE region = 'global' AND is_season_end = FALSE
                                       ORDER BY record_date
                                       ''')
        await message.edit(content='Select finished')
        with open('graphics/top_200_history.csv', 'w') as f:
            writer = csv.writer(f)
            headers = [column for column in records[0].keys()]
            writer.writerow(headers)
            for i, record in enumerate(records, 1):
                for data in zip(record['tags'], record['names'], record['trophies']):
                    data = [record['record_date']] + list(data)
                    writer.writerow(data)
        await message.edit(content='Here you go: https://clashnight.kuchenmampfer.de/top_200_history.csv')


def setup(bot):
    bot.add_cog(RunStuff(bot))
