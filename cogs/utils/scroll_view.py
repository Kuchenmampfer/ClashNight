from datetime import datetime

import discord
from discord import SelectOption
from matplotlib import pyplot as plt

from Locales import locales
from cogs.utils.TimePlot import TimePlot

LOCALIZATION = locales.get_data('utils')


class ScrollView(discord.ui.View):
    def __init__(self, plot: TimePlot, user_id: int, locale: str):
        self.plot = plot
        self.user_id = user_id
        self.locale = locale
        self.previous_button = discord.ui.Button(label='◀️', style=discord.ButtonStyle.green, row=1)
        self.previous_button.callback = self.got_to_previous_time_window
        self.next_button = discord.ui.Button(label='▶️', style=discord.ButtonStyle.green, row=1, disabled=True)
        self.next_button.callback = self.got_to_next_time_window
        self.now_button = discord.ui.Button(label='⏯️', style=discord.ButtonStyle.green, row=1, disabled=True)
        self.now_button.callback = self.got_to_current_time_window
        self.stop_button = discord.ui.Button(label='⏹️', style=discord.ButtonStyle.green, row=1)
        self.stop_button.callback = self.satisfied
        super().__init__(self.previous_button, self.next_button, self.now_button, self.stop_button,
                         disable_on_timeout=True)
        if len(self.plot.data) > 1:
            select_options = []
            names = []
            for tag, (name, _, _) in self.plot.data.items():
                select_options.append(SelectOption(label=name, description=tag, value=tag))
                names.append(name)
            if len(names) > 5:
                names = names[:5]
                names.append('...')
            select_options.insert(0, SelectOption(label=LOCALIZATION['all_accounts_select'][self.locale],
                                                  description=', '.join(names),
                                                  value='all accounts'))
            self.account_selector = discord.ui.Select(options=select_options, row=0)
            self.account_selector.callback = self.account_chosen
            self.add_item(self.account_selector)

    async def interaction_check(self, interaction: discord.Interaction) -> bool:
        if interaction.user.id == self.user_id:
            return True
        await interaction.response.send_message(LOCALIZATION['button_user_warning'][self.locale], ephemeral=True)
        return False

    async def got_to_previous_time_window(self, interaction: discord.Interaction):
        await interaction.response.defer()
        self.plot.previous()
        await self.update_message(interaction)

    async def got_to_next_time_window(self, interaction: discord.Interaction):
        await interaction.response.defer()
        self.plot.next()
        await self.update_message(interaction)

    async def got_to_current_time_window(self, interaction: discord.Interaction):
        await interaction.response.defer()
        self.plot.now()
        await self.update_message(interaction)

    async def satisfied(self, interaction: discord.Interaction):
        await interaction.response.defer()
        self.stop()
        await interaction.edit_original_response(view=None)

    async def account_chosen(self, interaction: discord.Interaction):
        await interaction.response.defer()
        self.plot.choose_account(self.account_selector.values[0])
        await self.update_message(interaction)

    async def update_message(self, interaction: discord.Interaction):
        self.previous_button.disabled = self.plot.current_end_time - self.plot.offset < self.plot.first_time
        self.next_button.disabled = self.plot.current_end_time + self.plot.offset > datetime.now(self.plot.timezone)
        self.now_button.disabled = self.plot.current_end_time == datetime.now(self.plot.timezone)
        await interaction.edit_original_response(content=self.plot.plot2url(), view=self)

    def on_timeout(self) -> None:
        plt.close(self.plot.fig)
