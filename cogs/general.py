import discord
from discord.ext import commands


class General(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(description='Read some general information about me')
    async def info(self, ctx: discord.ApplicationContext):
        await ctx.defer()
        description = 'This bot has its roots in the beginning of 2021, when my developer took a look into making a ' \
                      'discord bot in order to spam and climb the mee6 leaderboard in his server. But soon he ' \
                      'discovered that this was not possible and learned how to use the coc api and relational ' \
                      'databases instead. The bot grew, and in march 2022, it was finally ready to be released to the ' \
                      'public. Now everyone can see live in game leaderboards, track their trophy pushing progress ' \
                      'and more...\n\n' \
                      'To view my commands, use `/commands`. ' \
                      'If you like me, feel free to [invite me](' \
                      'https://discord.com/api/oauth2/authorize?client_id=854473040669966397&permissions=313344&scope' \
                      '=bot%20applications.commands) to all your servers. ' \
                      'If you need help, have found a bug, want to request a feature or stay up to date with the my ' \
                      'development, join my [support server](https://discord.gg/jSwB4ajUcq). ' \
                      'If you want to check out my source code, do so on [codeberg.org](' \
                      'https://codeberg.org/Kuchenmampfer/ClashNight). ' \
                      'If you want to support me financially, check out my [patreon](' \
                      'https://www.patreon.com/clashnight).'
        embed = discord.Embed(colour=discord.Colour.blue(), title='General Information about me',
                              description=description)
        async with self.bot.pool.acquire() as conn:
            accounts = await conn.fetchrow('SELECT COUNT(*) FROM RegisteredBuilderBasePlayers '
                                           'WHERE discord_member_id IS NOT NULL')
            embed.add_field(name='Servers', value=len(self.bot.guilds))
            embed.add_field(name='Users', value=len(self.bot.users))
            embed.add_field(name='Accounts tracked', value=accounts[0])
            support_server = self.bot.get_guild(949752401978589314)
            if support_server is not None:
                embed.set_thumbnail(url=support_server.icon.url)
            invite_button = discord.ui.Button(style=discord.ButtonStyle.link,
                                              label='invite me',
                                              url='https://discord.com/api/oauth2/authorize?client_id='
                                                  '854473040669966397&permissions=313344&scope=bot%20applications.commands')
            support_button = discord.ui.Button(style=discord.ButtonStyle.link,
                                               label='support server', url='https://discord.gg/jSwB4ajUcq')
            codeberg_button = discord.ui.Button(style=discord.ButtonStyle.link,
                                                label='code',
                                                url='https://codeberg.org/Kuchenmampfer/ClashNight')
            patreon_button = discord.ui.Button(style=discord.ButtonStyle.link,
                                               label='patreon', url='https://www.patreon.com/clashnight')
            view = discord.ui.View(invite_button, support_button, codeberg_button, patreon_button)
            await ctx.respond(embed=embed, view=view)

    @commands.slash_command(name='commands', description='Shows all my commands')
    async def show_commands(self, ctx: discord.ApplicationContext):
        await ctx.defer()
        embed = discord.Embed(colour=discord.Colour.blue(), title='My Commands')
        global_cmd_list = await self.bot.http.get_global_commands(self.bot.application_id)
        guild_cmd_list = await self.bot.http.get_guild_commands(self.bot.application_id, ctx.guild_id)
        for cmd in sorted(global_cmd_list + guild_cmd_list, key=lambda x: x['name']):
            if cmd["description"] != '':
                try:
                    options = cmd['options']
                except KeyError:
                    embed.add_field(name=f"/{cmd['name']}", value=cmd['description'], inline=False)
                    continue
                if all([option['type'] > 2 for option in options]):
                    embed.add_field(name=f"/{cmd['name']}", value=cmd['description'], inline=False)
                else:
                    for option in sorted(options, key=lambda x: x['name']):
                        if option['type'] <= 2:
                            embed.add_field(name=f"/{cmd['name']} {option['name']}", value=option['description'],
                                            inline=False)
        await ctx.respond(embed=embed)

    @commands.slash_command()
    async def invite(self, ctx: discord.ApplicationContext):
        """Invites and more"""
        e = discord.Embed(description=f'You can invite me with this [link](https://discord.com/api/oauth2/authorize?cli'
                                      f'ent_id=854473040669966397&permissions=313344&scope=bot%20applications.commands).\n '
                                      f'You can join the support server with this [link]'
                                      f'(https://discord.gg/jSwB4ajUcq).\n'
                                      f'In case you want to support us, you can do that on [Patreon]'
                                      f'(https://www.patreon.com/clashnight).', color=discord.Color.blue())
        await ctx.respond(embed=e)


def setup(bot):
    bot.add_cog(General(bot))
